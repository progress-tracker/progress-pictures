const mocha = require('mocha');
const chai = require('chai');
const chaiHttp = require('chai-http');

const server = require('../../index');

const should = chai.should();
chai.use(chaiHttp);

describe('GET @ /unknown/url', () => {
  it('should return 404 with non-empty error message', (done) => {
    chai.request(server)
    .get('/unknown/url')
    .end((err, res) => {
      res.should.have.status(404);
      res.body.should.have.property('error').not.eql('');
      done();
    });
  });
});

