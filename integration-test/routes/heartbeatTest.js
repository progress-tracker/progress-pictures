const chai = require('chai');
const chaiHttp = require('chai-http');

const server = require('../../index');
const config = require('../../config');

chai.use(chaiHttp);

describe('GET @ ' + config.web.baseUrl + '/heartbeat', () => {
  it('should return 200 with status available', (done) => {
    chai.request(server)
    .get(config.web.baseUrl + '/heartbeat')
    .end((err, res) => {
      res.should.have.status(200);
      res.body.should.have.property('status').eql('available');
      done();
    });
  });
});

