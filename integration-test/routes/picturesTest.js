const chai = require('chai');
const chaiHttp = require('chai-http');

const server = require('../../index');
const config = require('../../config');

chai.use(chaiHttp);

describe('POST @ ' + config.web.baseUrl + '/pictures', () => {
  it('should return 401 when no credentials', (done) => {
    chai.request(server)
    .post(config.web.baseUrl + '/pictures')
    .end((err, res) => {
      res.should.have.status(401);
      done();
    });
  });

  it('should return 401 when invalid credentials', (done) => {
    chai.request(server)
    .post(config.web.baseUrl + '/pictures')
    .set('Authorization', 'Basic ' + generateBasicAuthToken('wrong', 'wrong'))
    .end((err, res) => {
      res.should.have.status(401);
      done();
    });
  });
});

function generateBasicAuthToken(username, password) {
  return Buffer.from(username + ':' + password).toString('base64');
}

