const prodConfig = require('./src/configurations/prod');
const devConfig = require('./src/configurations/dev');

config = {};

if (process.env.NODE_ENV && process.env.NODE_ENV === 'production') {
  module.exports = prodConfig;
} else {
  module.exports = devConfig;
}
