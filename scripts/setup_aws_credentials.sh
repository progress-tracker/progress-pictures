#!/bin/sh

mkdir -p ~/.aws
touch ~/.aws/credentials

cat > ~/.aws/credentials << EOF
[default]
aws_access_key_id = ${AWS_ACCESS_KEY}
aws_secret_access_key = ${AWS_SECRET_KEY}
EOF
