#!/bin/sh

docker login -u gitlab-ci-token -p $CI_JOB_TOKEN registry.gitlab.com
docker build -t $GITLAB_IMAGE_NAME .
docker push $GITLAB_IMAGE_NAME
