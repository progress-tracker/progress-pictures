FROM node:latest

ENV NODE_ENV production
ENV APP_DIRECTORY /opt/progress-pictures/

ADD . ${APP_DIRECTORY}
WORKDIR ${APP_DIRECTORY}

RUN npm install

CMD [ "sh", "scripts/start_server.sh" ]
