const express = require('express');
const basicAuth = require('express-basic-auth');
const cors = require('cors');
const config = require('./config');

const notFoundParser = require('./src/middlewares/notFoundParser').parseNotFound;
const basicAuthHelper = require('./src/helpers/basicAuthHelper');

const heartbeat = require('./src/routes/heartbeat');
const pictures = require('./src/routes/pictures');

const app = express();
const router = express.Router();
const basicAuthMiddleware = basicAuth({
  authorizer: basicAuthHelper.validateCredentials,
  unauthorizedResponse: basicAuthHelper.unauthorizedResponse
});

router.get('/heartbeat', heartbeat.heartbeat);
router.post('/pictures', basicAuthMiddleware, pictures.pictures);

app.disable('x-powered-by');
app.use(config.web.baseUrl, router);
app.use(cors(config.web.corsOptions));
app.use(notFoundParser);

app.listen(config.web.port, () => {
  console.log('Server started listening on port ', config.web.port)
});

// For integration testing
module.exports = app;
