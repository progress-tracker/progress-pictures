config = {};

config.web = {};
config.web.baseUrl = '/api';
config.web.corsOptions = {
  origin: '*',  // Could later be changed to only allow requests from the API gateway
  methods: ['GET', 'POST', /*'PUT', 'DELETE'*/],
};

config.web.port = process.env.PORT;
config.web.basicAuthUsername = process.env.BASIC_AUTH_USERNAME;
config.web.basicAuthPassword = process.env.BASIC_AUTH_PASSWORD;

config.aws = {};
config.aws.bucketName = process.env.AWS_BUCKET_NAME;
config.aws.maxFileSize = 5 * 1024 * 1024 // 5 MB
config.aws.allowedFileExtensions = [
  '.jpg',
  '.jpeg',
  '.png'
];

module.exports = config;
