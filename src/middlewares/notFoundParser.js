module.exports = {
  parseNotFound: parseNotFound
};

function parseNotFound(req, res, next) {
  res.status(404).send({ error: 'Page not found' });
  next();
}
