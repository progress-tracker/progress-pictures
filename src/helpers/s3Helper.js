const config = require('../../config');

const multer = require('multer')
const multer_s3 = require('multer-s3');
const aws = require('aws-sdk');
const path = require('path');
const util = require('util');

module.exports = {
  getAllowedFormFields: getAllowedFormFields,
  generateMulterConfig: generateMulterConfig
};

function getAllowedFormFields() {
  return [
    { name: 'picture', maxCount: 1 }
  ];
}

function generateMulterConfig() {
  return multer({
    storage: multer_s3({
      s3: new aws.S3(),
      bucket: config.aws.bucketName,
      metadata: generateStorageMetadata,
      key: generateStorageKey
    }),
    limits: {
      fileSize: config.aws.maxFileSize
    },
    fileFilter: uploadRequestFilter
  });
}

function generateStorageMetadata(req, file, cb) {
  cb(null, {});
}

function generateStorageKey(req, file, cb) {
  var extension = path.extname(file.originalname);
  var storageKey = util.format(
    "%d%s",
    Date.now(),
    extension
  );

  cb(null, storageKey);
}

function uploadRequestFilter(req, file, cb) {
  try {
    validateRequest(req, file);
    cb(null, true);
  } catch (err) {
    cb(new Error(err));
  }
}

function validateRequest(req, file) {
  if (file === undefined) {
    throw 'no picture provided';
  }
  if (config.aws.allowedFileExtensions.indexOf(path.extname(file.originalname)) < 0) {
    throw 'invalid file extension';
  }
}
