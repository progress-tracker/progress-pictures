const config = require('../../config');

module.exports = {
  validateCredentials: validateCredentials,
  unauthorizedResponse: unauthorizedResponse
};

function validateCredentials(username, password) {
  return username == config.web.basicAuthUsername && password == config.web.basicAuthPassword;
}

function unauthorizedResponse(req) {
  return 'Access denied: invalid credentials';
}
