module.exports = {
  heartbeat: heartbeat
};

function heartbeat(req, res) {
  res.status(200).send({ status: 'available' });
}
