const s3Helper = require('../helpers/s3Helper');

module.exports = {
  pictures: pictures
};

var s3Upload = s3Helper.generateMulterConfig();

function pictures(req, res) {
  s3Upload.fields(s3Helper.getAllowedFormFields())(req, res, function (err) {
    if (err) {
      res.status(400).send({ message: err.message });
    } else if (!Object.keys(req.files).length) {
      res.status(400).send({ message: 'no picture provided' });
    } else {
      res.status(201).send({ message: 'picture successfully uploaded' });
    }
  });
}
