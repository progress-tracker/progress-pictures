const assert = require('assert');
const mocha = require('mocha');

const notFoundParser = require('../../src/middlewares/notFoundParser');

var actualStatus = null;
var actualResponse = null;
var nextCalled = false;

var mockResponse = {
  status: (s) => {
    actualStatus = s;
    return {
      send: (r) => actualResponse = r
    };
  }
};

var mockNext = () => nextCalled = true;

beforeEach(function () {
  actualStatus = null;
  actualResponse = null;
  nextCalled = false;
});

describe('NotFoundParser middleware', () => {
  describe('when parseNotFound', () => {
    it('should set status 404', () => {
      notFoundParser.parseNotFound(undefined, mockResponse, mockNext);
      assert.equal(actualStatus, 404);
    });

    it('should return a non-empty error message', () => {
      notFoundParser.parseNotFound(undefined, mockResponse, mockNext);
      assert.notEqual(actualResponse.error, '');
    });

    it('should pass the request to the next middleware', () => {
      notFoundParser.parseNotFound(undefined, mockResponse, mockNext);
      assert.equal(nextCalled, true);
    });
  });
});
