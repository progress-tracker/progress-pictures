const assert = require('assert');
const config = require('../../config');

const basicAuthHelper = require('../../src/helpers/basicAuthHelper');

describe('BasicAuthHelper helper', () => {
  describe('when validateCredentials', () => {
    it('should return false if username is invalid', () => {
      var result = basicAuthHelper.validateCredentials('wrong', config.web.basicAuthPassword);
      assert.equal(result, false);
    });

    it('should return false if password is invalid', () => {
      var result = basicAuthHelper.validateCredentials(config.web.basicAuthUsername, 'wrong');
      assert.equal(result, false);
    });

    it('should return false if username and password are invalid', () => {
      var result = basicAuthHelper.validateCredentials('wrong', 'wrong');
      assert.equal(result, false);
    });

    it('should return true if credentials are valid', () => {
      var result = basicAuthHelper.validateCredentials(config.web.basicAuthUsername, config.web.basicAuthPassword);
      assert.equal(result, true);
    });
  });

  describe('when unauthorizedResponse', () => {
    it('should return a non-empty error message', () => {
      var message = basicAuthHelper.unauthorizedResponse(null);
      assert.notEqual(message, undefined);
      assert.notEqual(message.length, 0);
    });
  });
});

