# progress-pictures

## API Documentation

### Heartbeat

* Route: GET `/api/heartbeat`
* Response:
  * Status:
    * 200 if the service is available
  * Body:

```json
{
  "status": "available"
}
```

### Pictures

* Route: POST `/api/pictures`
* Request:
  * Headers:
    * `Content-Type`: `multipart/form-data`
  * Body (as multipart form):

| Field name | Content |
| --- | --- |
| picture | The picture file |

* Response:
  * Status:
    * 201 if the picture is uploaded successfully
    * 400 otherwise
  * Body:

```json
{
  "message": "informative message"
}
```

**Currently supported pictures file extensions:**

* `.jpg`
* `.jpeg`
* `.png`
